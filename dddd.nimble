# Package

version       = "0.1.0"
author        = "coaljoe"
description   = "dddd"
license       = "Copyright"
bin           = @["main"]
srcDir        = "src"

# Dependencies

# ecs, nimasset
requires @["nim >= 0.14.2", "glm", "opengl", "nim-glfw", "collections", "stb_image >= 1.2", "struct",
  "nimlz4", "nimfp", "ecs", "ry", "nimasset"]

