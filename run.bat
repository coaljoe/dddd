@echo off
@rem cd to script's directory
cd %~dp0
setlocal
set DEBUG=1
set LOGLEVEL=DEBUG

@rem set apitrace path
set PATH=C:\Users\k\k\progs\apitrace\bin;%PATH%

@rem install ecs
@rem cd ..\ecs
@rem nimble install -y
@rem cd ..\ry


if "%1" == "-trace" (
  goto trace
) else if "%1" == "-release" (
  goto run_release
) else if "%1" == "" (
  goto run
) else (
  echo "unknown option: %1"
  goto end
)

:trace
shift
rem fixme, shift doesn't work with %*
rem echo %1 %2 %3 %4 %5
nim c main.nim
if %ERRORLEVEL% GEQ 1 EXIT /B 1
apitrace trace -o trace.trace main -v %1 %2 %3 %4 %5
goto end

:run
rem nim c -d:glfwDynlib -r main.nim
rem --verbosity:1 
nim --hint[Processing]:off --hint[Conf]:off ^
    --hint[Exec]:off  --nimcache:tmp/cache/nimcache ^
    --verbosity:1 --listCmd ^
    c -d:glfwDynlib --deadCodeElim:off --opt:none --threads:off --checks:on --warnings:on --debuginfo -r -o:main.exe src/main.nim %*
    rem c -d:glfwDynlib --deadCodeElim:off --opt:none --threads:off --checks:on --warnings:on --debuginfo --gc:markAndSweep -r main.nim %*
    rem c --opt:none --threads:off --checks:on --warnings:on --memTracker:on --debuginfo --gc:markAndSweep -r main.nim %*
    rem c --opt:none --threads:off --checks:on --warnings:on --memTracker:on --debuginfo -r main.nim %*
    rem c --opt:none --threads:off --checks:on --warnings:on --memTracker:on --debuginfo -d:useSysAssert -d:useGcAssert -r main.nim %*
    rem c --opt:none --threads:off --checks:on --warnings:on --memTracker:on --debuginfo --gc:markAndSweep -r main.nim %*
    rem c --opt:none --threads:off --checks:on --warnings:on --memTracker:on --debuginfo --gc:none -r main.nim %*
goto end

:run_release
nim --verbosity:2 --hint[Processing]:off --hint[Conf]:off ^
    --hint[Exec]:off --nimcache:tmp/cache/nimcache_release ^
    c -d:release -d:noAutoGLerrorCheck --threads:off --boundChecks:off --deadCodeElim:off --stackTrace:on -r -o:main.exe src/main.nim %*
    rem c -d:release --threads:off --boundChecks:off --deadCodeElim:off --stackTrace:on --gc:markAndSweep -r main.nim %*
goto end

:end
endlocal